# Botcheck timer for Amaranthine #

Automatically adds a small timer to the tab title and options bar in Amaranthine to countdown to next botcheck. Works in Tampermonkey (Chrome) and Greasemonkey (Firefox).

#### Download ####
[1 hour botcheck](https://bitbucket.org/breezeblock/botchecktimer/downloads/1hrtimer.user.js)

[30 minute botcheck](https://bitbucket.org/breezeblock/botchecktimer/downloads/30mintimer.user.js)

#### Grumpy approved ####

![alt](http://imgur.com/bfx7Qit.png)

![alt](http://imgur.com/umEFR1W.png)
