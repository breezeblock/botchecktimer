// ==UserScript==
// @name          Timer Thing 1 Hour
// @namespace	  timer1hr
// @description   1 Hour Botcheck timer
// @include       http://amar.bornofsnails.net/*
// @require       https://cdn.rawgit.com/robbmj/simple-js-countdown-timer/master/countdowntimer.js
// ==/UserScript==

var display = null,
    timer = new CountDownTimer(3600),
    timeObj = CountDownTimer.parse(3600),
    clock_obj = document.querySelector('#setting_button'),
    timer_html = document.createElement('span');

timer_html.innerHTML = "<span><button>Reset</button> Botcheck in: <span id='time'></span></span>";

clock_obj.parentNode.insertBefore(timer_html, clock_obj);
display = document.querySelector('#time');

format(timeObj.minutes, timeObj.seconds);
timer.onTick(format);
document.querySelector('button').addEventListener('click', function () {
  timer.start();
});

function format(minutes, seconds) {
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  display.textContent = minutes + ':' + seconds;
  document.title = 'Amaranthine: ' + minutes + ':' + seconds;
}